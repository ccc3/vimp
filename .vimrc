syntax on

"""""""""""""""
set ruler
set showmode
set encoding=utf8
set termencoding=utf8


set wmnu "affiche le menu
set wildmode=list:longest,list:full "affiche toutes les possibilitÚs
set wildignore=*.o,*.r,*.so,*.sl,*.tar,*.tgz "ignorer certains types de fichiers pour la complÚtion des includes

"""""""""""""""

set nu
set laststatus=2
set statusline=[%f]- 
"set statusline+=%#error# " todo highligths
set statusline+=%([%{Tlist_Get_Tagname_By_Line()}]%)
"set statusline+=%*       "normal highlight


set backspace=2
set tabstop=3
set shiftwidth=3
set expandtab
set mouse=nv

filetyp indent on

"wraped line"
noremap <buffer> <silent> <Up> gk
noremap <buffer> <silent> <Down> gj
noremap <buffer> <silent> <Home> g<Home>
noremap <buffer> <silent> <End> g<End>

inoremap <buffer> <silent> <Up> <C-o>gk
inoremap <buffer> <silent> <Down> <C-o>gj
inoremap <buffer> <silent> <Home> <C-o>g<Home>
inoremap <buffer> <silent> <End> <C-o>g<End>

au BufRead Makefile* set noexpandtab

set nocp

" ctags 
let g:ctags_statusline=1 
let generate_tags=1

call pathogen#infect()

" Commenting blocks of code.
autocmd FileType c,cpp,java,scala let b:comment_leader = '//'
autocmd FileType sh,ruby,python   let b:comment_leader = '#'
autocmd FileType conf,fstab       let b:comment_leader = '#'
autocmd FileType tex              let b:comment_leader = '%'
autocmd FileType mail             let b:comment_leader = '>'
autocmd FileType vim              let b:comment_leader = '"'

noremap <silent> <C-x> : <C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
 
map <F6> :TlistToggle<CR>

set foldmethod=syntax
set foldcolumn=4
au BufRead * normal zR

